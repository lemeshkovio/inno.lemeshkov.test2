package inno.lemeshkov.test2.repo;

import inno.lemeshkov.test2.models.Names;
import org.springframework.data.repository.CrudRepository;

public interface NamesRepo extends CrudRepository<Names, Integer> {
}
