package inno.lemeshkov.test2.controllers;

import inno.lemeshkov.test2.models.Names;
import inno.lemeshkov.test2.repo.NamesRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Optional;

@Controller
public class MainController {

    @Autowired
    NamesRepo namesRepo;

    @GetMapping("/")
    public String main(Model model){
        return "home";
    }

    @PostMapping("/")
    public String addName(@RequestParam String name, Model model){
        Names addName = new Names(name);
        namesRepo.save(addName);
        return "home";
    }

    @GetMapping("/{tableName}")
    public String getTable(@PathVariable(value = "tableName")String tableName, Model model){
        if (tableName.equals("names")){
            Iterable<Names> result = namesRepo.findAll();
            ArrayList<Names> names = new ArrayList<>();
            result.forEach(names::add);
            model.addAttribute("names", names);
        }
        return "names";
    }

    @GetMapping("/{tableName}/{id}")
    public String getTable(@PathVariable(value = "tableName")String tableName,@PathVariable(value = "id")Integer id, Model model){
        if (tableName.equals("names")) {
            Optional<Names> result = namesRepo.findById(id);
            ArrayList<Names> name = new ArrayList<>();
            result.ifPresent(name::add);
            model.addAttribute("names", name);
        }
        return "names";
    }
}
