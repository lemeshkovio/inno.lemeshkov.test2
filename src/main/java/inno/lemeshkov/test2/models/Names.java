package inno.lemeshkov.test2.models;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Names {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer nameId;

    private String name;

    public Names() {
    }

    public Names(String name) {
        this.name = name;
    }

    public Integer getNameId() {
        return nameId;
    }

    public void setNameId(Integer nameId) {
        this.nameId = nameId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
